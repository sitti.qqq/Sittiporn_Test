-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.17-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table test.tbl_grade
CREATE TABLE IF NOT EXISTS `tbl_grade` (
  `grade_id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(5) DEFAULT NULL,
  `grade_value` float DEFAULT NULL,
  PRIMARY KEY (`grade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table test.tbl_grade: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_grade` DISABLE KEYS */;
INSERT INTO `tbl_grade` (`grade_id`, `grade_name`, `grade_value`) VALUES
	(1, 'A', 4),
	(2, 'B+', 3.5),
	(3, 'B', 3),
	(4, 'C+', 2.5),
	(5, 'C', 2),
	(6, 'D+', 1.5),
	(7, 'D', 1),
	(8, 'F', 0);
/*!40000 ALTER TABLE `tbl_grade` ENABLE KEYS */;

-- Dumping structure for table test.tbl_subject
CREATE TABLE IF NOT EXISTS `tbl_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_number` varchar(10) DEFAULT NULL,
  `subject_fullname` varchar(50) DEFAULT NULL,
  `subject_credit` int(11) DEFAULT NULL,
  `subject_year` int(11) DEFAULT NULL,
  `subject_semester` int(11) DEFAULT NULL,
  `subject_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table test.tbl_subject: ~13 rows (approximately)
/*!40000 ALTER TABLE `tbl_subject` DISABLE KEYS */;
INSERT INTO `tbl_subject` (`subject_id`, `subject_number`, `subject_fullname`, `subject_credit`, `subject_year`, `subject_semester`, `subject_status`) VALUES
	(1, '3002001', 'Englist for scientist 1', 3, 1, 1, 1),
	(2, '5003001', 'Mathematics 1', 3, 1, 1, 1),
	(3, '5003004', 'General Chemistry', 3, 1, 1, 1),
	(4, '5003005', 'General Chemistry Laboratory', 1, 1, 1, 1),
	(5, '5003006', 'General Physics', 3, 1, 1, 1),
	(6, '5003007', 'General Physics Laboratory', 1, 1, 1, 1),
	(7, '5003008', 'Elementary Statistics', 3, 1, 1, 1),
	(8, '5076001', 'Programming Fundamentals', 3, 1, 1, 1),
	(9, '5076004', 'Data Structures and Algorithms', 3, 1, 2, 1),
	(10, '5076005', 'Computer Organization and Architectures', 3, 1, 2, 1),
	(11, '5076006', 'Automata Theory and Programming Lanuages', 3, 1, 2, 1),
	(12, '5076010', 'Operating Systems', 3, 1, 2, 1),
	(13, '5076011', 'Database Systems', 3, 1, 2, 1);
/*!40000 ALTER TABLE `tbl_subject` ENABLE KEYS */;

-- Dumping structure for table test.tbl_subject_value
CREATE TABLE IF NOT EXISTS `tbl_subject_value` (
  `sv_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_subject_id` int(11) DEFAULT NULL,
  `fk_grade_id` int(11) DEFAULT NULL,
  `sv_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`sv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table test.tbl_subject_value: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_subject_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_subject_value` ENABLE KEYS */;

-- Dumping structure for table test.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `user_datetime_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table test.tbl_user: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_datetime_create`, `user_status`) VALUES
	(1, '3453!', '2018-02-22 18:37:18', 1),
	(2, 'sittiporn', '2018-02-22 19:01:15', 1),
	(3, 'asdsad', '2018-02-22 19:02:06', 1),
	(4, NULL, '2018-02-22 19:26:18', 1);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
