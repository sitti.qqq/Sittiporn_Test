<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sittiporn Test</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/assets/devicons/css/devicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>/assets/css/resume.min.css" rel="stylesheet">

    </head>

    <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-block d-lg-none">Start Bootstrap</span>
                <span class="d-none d-lg-block">
                    <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="<?php echo base_url(); ?>/image/grade.jpg" alt="">
                </span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#experience">Project</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">About</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid p-0">
            <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="experience">
                <div class="my-auto">
                    <h2 class="mb-1">ระบบคำนวณเกรด
                        <span class="text-primary">เบื้องต้น</span>
                    </h2>
                    <hr>
                    <div class="col-sm-12 p-1" id="login">
                        <?php echo form_open('',array('id' => 'form_user')); ?>
                        <div class="col-sm-3 p-1">
                            <input type="text" name="username" id="username" placeholder="กรุณาใส่ชื่อเพื่อเข้าใช้งาน" class="form-control onlytext"  maxlength="40" required>
                        </div>
                        <div class="col-sm-3 p-1 text-right" >
                            <button type="submit" class="btn btn-warning">Sign in</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="col-sm-12 p-1" id="logout" >     
                        <div class="col-sm-12 p-1" >
                            <h3 class="mb-0"><span class="text-success"><label id="label_name"></label></span></h3>
                        </div>
                        <div class="col-sm-3 p-1 text-right" >
                            <button type="button" class="btn btn-danger" id="sign_out">Sign Out</button>
                        </div>
                    </div>


                    <div class="col-sm-12 p-3" id="table"></div>

                </div>

            </section>

            <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
                <div class="my-auto">
                    <h1 class="mb-0">Sittiporn
                        <span class="text-primary">Leklong</span>
                    </h1>
                    <div class="subheading mb-5">Tel : 080-554-1243 ·
                        <a href="mailto:sitti.kiv@hotmail.com">Sitti.kiv@hotmail.com</a>
                    </div>
                    <!--                    <p class="mb-5">แบบฝึกหัดการพัฒนาระบบเกรดเบื้องต้น.</p>-->
                </div>
            </section>

        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo base_url(); ?>/assets/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>/assets/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for this template -->
        <script src="<?php echo base_url(); ?>/assets/js/resume.min.js"></script>

    </body>
    <script>
        $(function(){
            $('#logout').hide();
            $('body').on('click','#sign_out',function(){
                $('#table').html('');
                $('#login').slideDown();
                $('#logout').hide();
            });

            $('body').on('submit','#form_user',function(e){
                e.preventDefault();
                $.ajax( {
                    url: '<?php echo base_url('Home/check_user');?>',
                    type: 'POST',
                    data: new FormData( this ),
                    contentType: false,
                    processData: false,
                    success: function(get_data){
                        $('#login').slideUp();
                        $('#label_name').html('สวัสดีคุณ '+$('#username').val());
                        $('#logout').slideDown();
                        get_table(get_data);
                    },
                } );
                return false;
            });
            $('.onlytext').keypress(function (key) {
                if ((key.charCode < 64 || key.charCode > 122) && (key.charCode < 3585 || key.charCode > 3630) && (key.charCode < 3632 || key.charCode > 3641) && (key.charCode < 3647 || key.charCode > 3660) && (key.charCode != 13) && (key.charCode < 3665 || key.charCode > 3673) && (key.charCode != 33) && (key.charCode == 35) && (key.charCode != 32) && (key.charCode != 0) && (key.charCode != 36) && (key.charCode != 37) && (key.charCode < 45 || key.charCode > 57)) {
                    return false;
                }
            });
            $('.onlytext').blur(function () {
                var $th = $(this);
                $th.val($th.val().replace(/[^a-z A-Z0-9ก-ฮะาิีุูเะแำไโ๑๒๓๔๕๖๗๘๙๐ใๆ่้๊๋ั็์ึื%$/@.\-฿_]/g, function (str) {
                    return '';
                }));
            });
        });




        $('body').on('change','.select_grade',function(){
            var grade_id = $(this).val(); 
            var subject_id = $(this).data('subject_id'); 
            var user_id = $('#user_id').val(); 
            $("#tr_id_"+subject_id).removeClass("text-danger");
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('Home/update_grade');?>",
                data: {grade_id:grade_id,subject_id:subject_id,user_id:user_id},
                success: function (data)
                {
                    get_table(user_id)
                }
            }); 
        });
        function get_table(user_id){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('Home/table_grade');?>",
                data: {user_id:user_id},
                success: function (data)
                {
                    $('#table').html(data);
                }
            }); 
        }
    </script>

</html>
