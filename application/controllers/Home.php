<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Model_grade');
    }

    public function index()
    {
        $this->load->view('Home');
    }
    public function check_user()
    {
        $name = $this->input->post('username');
        $where_name = array(
            'user_name'=> $name,
            'user_status'=> 1,
        );
        $chk_user = $this->Model_grade->chk_user($where_name);

        if(!empty($chk_user)){
            echo ($chk_user[0]['user_id']);
        }else{
            $data_insert = array(
                'user_name' => $name,
            );
            $user_id =  $this->Model_grade->insert_user($data_insert);
            $where_id = array(
                'user_id'=> $user_id,
                'user_status'=> 1,
            );
            $chk_user =  $this->Model_grade->chk_user($where_id);
            echo ($chk_user[0]['user_id']);
        }
    }
    public function update_grade(){
        $subject_id = $this->input->post('subject_id');
        $grade_id = $this->input->post('grade_id');
        $user_id = $this->input->post('user_id');
        $data = array(
            'fk_subject_id' => $subject_id,
            'fk_user_id' => $user_id,
            'fk_grade_id' => $grade_id,
        );
        $where_data = array(
            'fk_subject_id' => $subject_id,
            'fk_user_id' => $user_id,
        );
        $check_value = $this->Model_grade->check_value($where_data);

        if(!empty($check_value)){
            $where_update = array('sv_id' => $check_value[0]['sv_id']);
            $this->Model_grade->update_value($where_update,$data);
        }else{

            $this->Model_grade->insert_value($data);
        }
    }
    public function table_grade(){

        $user_id = $this->input->post('user_id');
        $subject = $this->Model_grade->get_subject($user_id);
        $grade = $this->Model_grade->get_grade();
        $subject_semester = $this->Model_grade->get_subject_semester();
        $html = '';
        $credit = array();
        $total_grade = array();
        if(!empty($subject_semester)){
            foreach($subject_semester as $se){
                if(!empty($subject['subject'])){
                    $credit[$se['subject_semester']] = 0;
                    $total_grade[$se['subject_semester']] = 0;

                    $html .='
                        <div class="table-responsive" style="border-radius: 20px">
                            <input type="hidden" id="user_id" value="'.$user_id.'">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr style="background:#f4c891">
                                        <th  class="text-center" rowspan="" colspan="8">ภาคการศึกษาที่ '.$se['subject_semester'].'</th>
                                    </tr>
                                    <tr style="background:#f4c891">
                                        <th class="text-center">รหัสวิชา</th>
                                        <th class="text-center">ชื่อวิชา</th>
                                        <th class="text-center">หน่วยกิต</th>
                                        <th class="text-center">ปีที่</th>
                                        <th class="text-center">เกรด</th>
                                    </tr>
                                </thead>
                                <tbody>';
                    foreach($subject['subject'] as $s){
                        if($s['subject_semester'] == $se['subject_semester']){
                            $credit[$se['subject_semester']] += $s['subject_credit']; 
                          
                            if(empty($subject['value'][$s['subject_id']]['grade_id'])){
                                $text_red = 'text-danger';
                                $grade_id = '';
                            }else{
                                $text_red = '';
                                $grade_id = $subject['value'][$s['subject_id']]['grade_id'];
                                $total_grade[$se['subject_semester']] += $subject['value'][$s['subject_id']]['total_grade'];
                            }
                            $html .='<tr class="'.$text_red.'" id="tr_id_'.$s['subject_id'].'">
                                        <td class="text-center">'.$s['subject_number'].'</td>
                                        <td class="text-left">'.$s['subject_fullname'].'</td>
                                        <td class="text-center">'.$s['subject_credit'].'</td>
                                        <td class="text-center">'.$s['subject_year'].'</td>
                                        <td class="text-center">
                                            <select class="form-control select_grade" data-subject_id="'.$s['subject_id'].'">
                                                <option ></option>';
                            if(!empty($grade)){
                                foreach($grade as $g){ 
                                    $selected = '';
                                    if($g['grade_id'] == $grade_id){
                                        $selected = 'selected';
                                    }
                                    $html .='<option value="'.$g['grade_id'].'" '.$selected.'>'.$g['grade_name'].'</option>';

                                }
                            }

                            $html .='</select> 
                                        </td>
                                    </tr>';
                        } 
                    }
                      $sum_credit = 0;
                            $sum_total_grade = 0;
                            foreach($credit as $key => $cc){
                                $sum_credit +=  $cc;
                            }
                            foreach($total_grade as $key => $g){
                                $sum_total_grade +=  $g;
                            }
                    $html .='</tbody>
                                <tbody id="semester_'.$se['subject_semester'].'">
                                    <tr style="background:#bdd8ec">
                                        <td  class="text-center">ผลรวม</td>
                                        <td class="text-center">หน่วยกิตรวม</td>
                                        <td class="text-center" colspan="3">GPA</td>
                                    </tr>
                                    <tr>
                                        <td  class="text-center">ประจำภาค</td>
                                        <!--                                    <td class="text-center"></td>-->
                                        <td class="text-center">'.$credit[$se['subject_semester']].'</td>
                                        <!--                                    <td class="text-center">GPA</td>-->
                                        <td class="text-center" colspan="3">'.number_format($total_grade[$se['subject_semester']]/$credit[$se['subject_semester']],2).'</td>
                                    </tr>
                                    <tr>
                                        <td  class="text-center">สะสม</td>
                                        <!--                                    <td class="text-center"></td>-->
                                        <td class="text-center">'.$sum_credit.'</td>
                                        <!--                                    <td class="text-center">GPA</td>-->
                                        <td class="text-center" colspan="3">'.number_format($sum_total_grade/$sum_credit,2).'</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>';

                }
            } 
            echo $html;
        }
    } 
}
