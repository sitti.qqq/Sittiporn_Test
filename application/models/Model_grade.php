<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_grade extends CI_Model {

    function get_subject($user_id){
        $where = array(
            'fk_user_id' => $user_id,
        );
        $this->db->where($where);
        $value = $this->db
            ->join('tbl_grade','grade_id=fk_grade_id')
            ->join('tbl_subject','subject_id=fk_subject_id')
            ->get('tbl_subject_value')->result_array();
        $arr_value = array();
        if(!empty($value)){
            foreach($value as $v){
                $total_grade = $v['grade_value'] * $v['subject_credit'];
                $arr_value[$v['fk_subject_id']] = array(
                    'grade_id' => $v['fk_grade_id'],
                    'grade_value' => $v['grade_value'],
                    'subject_credit' => $v['subject_credit'],
                    'total_grade' => $total_grade,
                );
            }
        }
        $subject = $this->db
            ->where('subject_year',1)
            ->get('tbl_subject')->result_array();
        $data = array(
            'subject' => $subject,
            'value' => $arr_value,
        );
        return $data;


    }  
    function check_value($where){
        return $this->db->where($where)->get('tbl_subject_value')->result_array();

    }  
    function update_value($where,$data){
        $this->db->where($where)->update('tbl_subject_value',$data);

    }
    function insert_value($data){
        $this->db->insert('tbl_subject_value',$data);

    }
    function get_grade(){
        $data = $this->db->get('tbl_grade')->result_array();
        return $data;
    } 
    function get_subject_semester(){
        $data = $this->db->select('subject_semester')->where('subject_year',1)->group_by('subject_semester')->get('tbl_subject')->result_array();
        return $data;
    }
    function chk_user($where){
        return $this->db->where($where)->get('tbl_user')->result_array();
    }
    function insert_user($data){
        $this->db->insert('tbl_user',$data);
        return $this->db->insert_id();
    }
}
?>